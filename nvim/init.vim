" Vim-plug source
source $HOME/.config/nvim/plugins/plugins.vim

" Configuration files
lua require'nvim-treesitter.configs'.setup{highlight={enable=true}}
lua require('config/init')
