-- General editor configuration

vim.opt.syntax = 'true'
vim.cmd.colorscheme('everforest')
vim.cmd.highlight({ 'Normal', 'guibg=NONE', 'ctermbg=NONE' })
vim.cmd.highlight({ 'NormalFloat', 'guibg=NONE' })
vim.cmd.highlight({ 'EndOfBuffer', 'guibg=NONE' })
vim.opt.termguicolors = true

vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.scrolloff = 8
vim.opt.updatetime = 50
vim.opt.colorcolumn = "80"

vim.opt.swapfile = false
vim.opt.wrap = false
vim.opt.undofile = true
vim.opt.undodir = os.getenv('HOME') .. '/.local/share/nvim/undodir'

-- Remaps

vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "<leader>uu", ":PlugUpdate<CR>")
vim.keymap.set("n", "<leader>ui", ":PlugInstall<CR>")
