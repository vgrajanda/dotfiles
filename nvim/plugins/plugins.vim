call plug#begin()

" Better syntax support
Plug 'sheerun/vim-polyglot'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Telescope

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.5' }

" Auto pairs 
Plug 'jiangmiao/auto-pairs'

" Colors & Icons
Plug 'flazz/vim-colorschemes'
Plug 'sainnhe/everforest'
Plug 'nvim-tree/nvim-web-devicons'


call plug#end()
